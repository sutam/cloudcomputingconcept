/**********************************
 * FILE NAME: MP2Node.cpp
 *
 * DESCRIPTION: MP2Node class definition
 **********************************/
#include "MP2Node.h"

/**
 * constructor
 */
MP2Node::MP2Node(Member *memberNode, Params *par, EmulNet * emulNet, Log * log, Address * address) {
	this->memberNode = memberNode;
	this->par = par;
	this->emulNet = emulNet;
	this->log = log;
	ht = new HashTable();
	this->memberNode->addr = *address;
        this->transID = 0;
}

/**
 * Destructor
 */
MP2Node::~MP2Node() {
	delete ht;
	delete memberNode;
}
/**
 * FUNCTION NAME: ringIsChanged 
 *
 * DESCRIPTION: This function does the following:
 *                   Compare the ring with the updated ring
 *                   to find out if any node failed or updated
 */
bool MP2Node::ringIsChanged( vector<Node> newRing ) {

     vector<Node> v( sizeof(ring) );
     vector<Node>::iterator it = std::set_difference ( ring.begin(), ring.end(), newRing.begin(), newRing.end(),
                           v.begin() );
     v.resize( it - v.begin() );
     return ( ! ( v.size() == 0 ) );
}

/**
 * FUNCTION NAME: updateRing
 *
 * DESCRIPTION: This function does the following:
 * 				1) Gets the current membership list from the Membership Protocol (MP1Node)
 * 				   The membership list is returned as a vector of Nodes. See Node class in Node.h
 * 				2) Constructs the ring based on the membership list
 * 				3) Calls the Stabilization Protocol
 */
void MP2Node::updateRing() {
	/*
	 * Implement this. Parts of it are already implemented
	 */
	vector<Node> curMemList;
	bool change = false;

	/*
	 *  Step 1. Get the current membership list from Membership Protocol / MP1
	 */
	curMemList = getMembershipList();
	/*
	 * Step 2: Construct the ring
	 */
	// Sort the list based on the hashCode
	sort(curMemList.begin(), curMemList.end());

	/*
	 * Step 3: Run the stabilization protocol IF REQUIRED
	 */
	// Run stabilization protocol if the hash table size is greater than zero and if there has been a changed in the ring
        change = ringIsChanged( curMemList );
        if (ring.size() != 0 && (ring.size() != curMemList.size() || change) )
            stabilizationProtocol();
        // else
        // just remain status quo

#ifdef DEBUGLOG
       string ip = "";
       vector<Node>::iterator it;
       if (change)
       {
       	   log->LOG( &memberNode->addr, "updateRing() called" );
           log->LOG( &memberNode->addr, "ring before updateRing" );
           for  ( it = ring.begin(); it != ring.end(); ++it )
           {
              Address *addr = it->getAddress();
              ip += getIP (addr );
           }
           log->LOG( &memberNode->addr, ip.c_str() );

           ip = "";
           for  ( it = hasMyReplicas.begin(); it != hasMyReplicas.end(); ++it )
           {
              Address *addr = it->getAddress();
              ip += getIP (addr );
           }
           log->LOG( &memberNode->addr, "replica %s", ip.c_str() );
       }
#endif
        ring = curMemList;
        UpdateReplicaList();
#ifdef DEBUGLOG
        if (change)
        {
            log->LOG( &memberNode->addr, "ring after updateRing()" );
            ip = "";
            for  ( it = ring.begin(); it != ring.end(); ++it )
            {
                   Address *addr = it->getAddress();
                   ip += getIP( addr );
            }
            log->LOG( &memberNode->addr, ip.c_str() );

            ip = "";
            for  ( it = hasMyReplicas.begin(); it != hasMyReplicas.end(); ++it )
            {
              Address *addr = it->getAddress();
              ip += getIP (addr );
            }
            log->LOG( &memberNode->addr, "replica %s", ip.c_str() );
        }
#endif
}

/**
 * FUNCTION NAME: getMemberhipList
 *
 * DESCRIPTION: This function goes through the membership list from the Membership protocol/MP1 and
 * 				i) generates the hash code for each member
 * 				ii) populates the ring member in MP2Node class
 * 				It returns a vector of Nodes. Each element in the vector contain the following fields:
 * 				a) Address of the node
 * 				b) Hash code obtained by consistent hashing of the Address
 */
vector<Node> MP2Node::getMembershipList() {
	unsigned int i;
	vector<Node> curMemList;
	for ( i = 0 ; i < this->memberNode->memberList.size(); i++ ) {
		Address addressOfThisMember;
		int id = this->memberNode->memberList.at(i).getid();
		short port = this->memberNode->memberList.at(i).getport();
		memcpy(&addressOfThisMember.addr[0], &id, sizeof(int));
		memcpy(&addressOfThisMember.addr[4], &port, sizeof(short));
		curMemList.emplace_back(Node(addressOfThisMember));
	}
	return curMemList;
}

/**
 * FUNCTION NAME: hashFunction
 *
 * DESCRIPTION: This functions hashes the key and returns the position on the ring
 * 				HASH FUNCTION USED FOR CONSISTENT HASHING
 *
 * RETURNS:
 * size_t position on the ring
 */
size_t MP2Node::hashFunction(string key) {
	std::hash<string> hashFunc;
	size_t ret = hashFunc(key);
	return ret%RING_SIZE;
}

/**
 * FUNCTION NAME: clientCreate
 *
 * DESCRIPTION: client side CREATE API
 * 				The function does the following:
 * 				1) Constructs the message
 * 				2) Finds the replicas of this key
 * 				3) Sends a message to the replica
 */
void MP2Node::clientCreate(string key, string value) {
	/*
	 * Implement this
	 */
        /* Start by logging this transaction */
        g_transID++;
        struct transaction transaction;
        transaction.transID = g_transID;
        transaction.transType = CREATE;
        transaction.replyCount = 0;
        transaction.quorumCount = 0;
        transaction.status = 0;
        transaction.key = key;
        transaction.value = value;
        transaction.timestamp = par->getcurrtime();
        transactionTable[g_transID] = transaction;
	
        /* Construct the message */
	Address address;
        int id     = *(int *) (&memberNode->addr);
        short port = *(short *) (&memberNode->addr.addr[4]);
	memcpy(&address.addr[0], &id, sizeof(int));
	memcpy(&address.addr[4], &port, sizeof(short));

        /* Send message to replicas */
        vector<Node> Replicas = findNodes(key);
        for (unsigned int i = 0; i < Replicas.size(); i++ )
        {
              Node replica = Replicas[i];
              ReplicaType type;
              switch  (i) 
              {
                 case 0:
                     type = PRIMARY;
                     break;
                 case 1:
                     type = SECONDARY;
                     break;
                 case 2:
                     type = TERTIARY;
                     break;
                 default:
                     continue;
                     break;
               }
#ifdef DEBUGLOG
               //log->LOG(&memberNode->addr, "Create transaction ID %d in hashtable", g_transID);
#endif
               Message MessageCreate ( g_transID, address, CREATE, key, value, type);
               string message = MessageCreate.toString();
               emulNet->ENsend( &memberNode->addr, replica.getAddress(), (char*) message.c_str(), message.size() );
        }
}

/**
 * FUNCTION NAME: clientRead
 *
 * DESCRIPTION: client side READ API
 * 				The function does the following:
 * 				1) Constructs the message
 * 				2) Finds the replicas of this key
 * 				3) Sends a message to the replica
 */
void MP2Node::clientRead(string key){
	/*
	 * Implement this
	 */
        /* Construct the message */
        /* Start by logging this transaction */
        g_transID++;
        struct transaction transaction;
        transaction.transID = g_transID;
        transaction.transType = READ;
        transaction.replyCount = 0;
        transaction.quorumCount = 0;
        transaction.status = 0;
        transaction.key = key;
        transaction.value = "";
        transaction.timestamp = par->getcurrtime();
        transactionTable[g_transID] = transaction;

	Address address;
        int id     = *(int *) (&memberNode->addr);
        short port = *(short *) (&memberNode->addr.addr[4]);
	memcpy(&address.addr[0], &id, sizeof(int));
	memcpy(&address.addr[4], &port, sizeof(short));

        vector<Node> Replicas = findNodes(key);
        for (unsigned int i = 0; i < Replicas.size(); i++ )
        {
              Node replica = Replicas[i];
              Message MessageRead( g_transID, address, READ, key );
              string message = MessageRead.toString();
              emulNet->ENsend( &memberNode->addr, replica.getAddress(), (char*) message.c_str(), message.size() );
        }
}

/**
 * FUNCTION NAME: clientUpdate
 *
 * DESCRIPTION: client side UPDATE API
 * 				The function does the following:
 * 				1) Constructs the message
 * 				2) Finds the replicas of this key
 * 				3) Sends a message to the replica
 */
void MP2Node::clientUpdate(string key, string value){
	/*
	 * Implement this
	 */
        /* Start by logging this transaction */
        g_transID++;
        struct transaction transaction;
        transaction.transID = g_transID;
        transaction.transType = UPDATE;
        transaction.replyCount = 0;
        transaction.quorumCount = 0;
        transaction.status = 0;
        transaction.key = key;
        transaction.value = value;
        transaction.timestamp = par->getcurrtime();
        transactionTable[g_transID] = transaction;

        /* Construct the message */
	Address address;
        int id     = *(int *) (&memberNode->addr);
        short port = *(short *) (&memberNode->addr.addr[4]);
	memcpy(&address.addr[0], &id, sizeof(int));
	memcpy(&address.addr[4], &port, sizeof(short));

        vector<Node> Replicas = findNodes(key);
        for (unsigned int i = 0; i < Replicas.size(); i++ )
        {
              Node replica = Replicas[i];
              ReplicaType type;
              switch ( i )
              {
                 case 0:
                     type = PRIMARY;
                     break;
                 case 1:
                     type = SECONDARY;
                     break;
                 case 2:
                     type = TERTIARY;
                     break;
                 default:
                     continue;
                     break;
               }
               Message MessageUpdate( g_transID, address, UPDATE, key, value, type);
               string message = MessageUpdate.toString();
               emulNet->ENsend( &memberNode->addr, replica.getAddress(), (char*) message.c_str(), message.size() );
        }
}

/**
 * FUNCTION NAME: clientDelete
 *
 * DESCRIPTION: client side DELETE API
 * 				The function does the following:
 * 				1) Constructs the message
 * 				2) Finds the replicas of this key
 * 				3) Sends a message to the replica
 */
void MP2Node::clientDelete(string key){
	/*
	 * Implement this
	 */
        /* Start by logging this transaction */
        g_transID++;
        struct transaction transaction;
        transaction.transID = g_transID;
        transaction.transType = DELETE;
        transaction.replyCount = 0;
        transaction.quorumCount = 0;
        transaction.status = 0;
        transaction.key = key;
        transaction.timestamp = par->getcurrtime();
        transactionTable[g_transID] = transaction;

        /* Construct the message */
	Address address;
        int id     = *(int *) (&memberNode->addr);
        short port = *(short *) (&memberNode->addr.addr[4]);
	memcpy(&address.addr[0], &id, sizeof(int));
	memcpy(&address.addr[4], &port, sizeof(short));

        vector<Node> Replicas = findNodes(key);
        for (unsigned int i = 0; i < Replicas.size(); i++ )
        {
              Node replica = Replicas[i];
              Message MessageDelete( g_transID, address, DELETE, key);
              string message = MessageDelete.toString();
              emulNet->ENsend( &memberNode->addr, replica.getAddress(), (char*) message.c_str(), message.size() );
        }
}

/**
 * FUNCTION NAME: createKeyValue
 *
 * DESCRIPTION: Server side CREATE API
 * 			   	The function does the following:
 * 			   	1) Inserts key value into the local hash table
 * 			   	2) Return true or false based on success or failure
 */
bool MP2Node::createKeyValue(string key, string value, ReplicaType replica) {
	/*
	 * Implement this
	 */
	// Insert key, value, replicaType into the hash table
        if ( !ht->isEmpty() )
        {
             if ( ! (ht->read( key )).empty() )
                  return false;
        }
        Entry entry( value, par->getcurrtime(), replica );
        return ht->create( key, entry.convertToString() );
}

/**
 * FUNCTION NAME: readKey
 *
 * DESCRIPTION: Server side READ API
 * 			    This function does the following:
 * 			    1) Read key from local hash table
 * 			    2) Return value
 */
string MP2Node::readKey(string key) {
	/*
	 * Implement this
	 */
	// Read key from local hash table and return value
        string entryString = ht->read ( key );
        if ( entryString.empty() ) return entryString;
        Entry entry( entryString );
        return entry.value;
}

/**
 * FUNCTION NAME: updateKeyValue
 *
 * DESCRIPTION: Server side UPDATE API
 * 				This function does the following:
 * 				1) Update the key to the new value in the local hash table
 * 				2) Return true or false based on success or failure
 */
bool MP2Node::updateKeyValue(string key, string value, ReplicaType replica) {
	/*
	 * Implement this
	 */
	// Update key in local hash table and return true or false
        Entry entry( value, par->getcurrtime(), replica );
        return ht->update( key, entry.convertToString() );
}

/**
 * FUNCTION NAME: deleteKey
 *
 * DESCRIPTION: Server side DELETE API
 * 				This function does the following:
 * 				1) Delete the key from the local hash table
 * 				2) Return true or false based on success or failure
 */
bool MP2Node::deleteKey(string key) {
	/*
	 * Implement this
	 */
	// Delete the key from the local hash table
        return ht->deleteKey( key );
}

/**
 * FUNCTION NAME: ReplyAndUpdateTable 
 *
 * DESCRIPTION: This function is the transaction table handler of this node.
 */
void MP2Node::ReplyAndUpdateTable( Message transMessage )
{
        map<int, struct transaction>::iterator clientTransaction= transactionTable.find(transMessage.transID);
        if ( clientTransaction == transactionTable.end() )
        {
#ifdef DEBUGLOG
             //log->LOG(&memberNode->addr, "unmapped transID %d", transMessage.transID );
             return;
#endif
        }
        struct transaction *transaction = &clientTransaction->second;
        switch (transMessage.type)
        {
               case REPLY:
               {
                    transaction->replyCount++;
                    if (transMessage.success)
                    {
                        transaction->quorumCount++;
                        if (transaction->quorumCount >= 2 )
                        {
                            switch (transaction->transType)
                            { 
                               case CREATE:
                                    log->logCreateSuccess( &memberNode->addr, true, transMessage.transID, transaction->key, transaction->value );
                                    break;
                               case UPDATE:
                                    log->logUpdateSuccess( &memberNode->addr, true, transMessage.transID, transaction->key, transaction->value );
                                    break;
                               case DELETE:
                                    log->logDeleteSuccess( &memberNode->addr, true, transMessage.transID, transaction->key );
                                    break;
                                default:
                                    break;
                            }
#ifdef DEBUGLOG
             log->LOG(&memberNode->addr, "removing transaction %d", transMessage.transID );
#endif
                            transactionTable.erase( clientTransaction);
                        }
                    } 
                    else {
                       if (transaction->replyCount > 2 )
                       {
                           if (transaction->quorumCount < 2 )
                           {
                               switch (transaction->transType)
                               {
                                  case CREATE:
                                     log->logCreateFail( &memberNode->addr, true, transMessage.transID, transaction->key, transaction->value );
                                     break;
                                  case UPDATE:
                                     log->logUpdateFail( &memberNode->addr, true, transMessage.transID, transaction->key, transaction->value );
                                     break;
                                  case DELETE:
                                     log->logDeleteFail( &memberNode->addr, true, transMessage.transID, transaction->key );
                                     break;
                                  default:
                                     break;
                               }
#ifdef DEBUGLOG
             log->LOG(&memberNode->addr, "removing transaction %d", transMessage.transID );
#endif
                               transactionTable.erase( clientTransaction );
                           }
                       }
                    } 
                    break;
               }
               case READREPLY:
               {
                    transaction->replyCount++;
                    if (!transMessage.value.empty())
                    {
                        transaction->quorumCount++;
                        if (transaction->quorumCount >= 2 )
                        {
                            log->logReadSuccess( &memberNode->addr, true, transMessage.transID, transaction->key, transMessage.value );
#ifdef DEBUGLOG
             log->LOG(&memberNode->addr, "removing transaction %d", transMessage.transID );
#endif
                            transactionTable.erase( clientTransaction );
                        }
                    }  else {
                       if (transaction->replyCount > 2 )
                       {
                           if (transaction->quorumCount < 2 )
                           {
                               log->logReadFail( &memberNode->addr, true, transMessage.transID, transaction->key);
#ifdef DEBUGLOG
             log->LOG(&memberNode->addr, "removing transaction %d", transMessage.transID );
#endif
                               transactionTable.erase( clientTransaction );
                           }
                       }
                    } 
                    break;
               }         
               default:
                    break;
         }
}

/**
 * FUNCTION NAME: checkMessages
 *
 * DESCRIPTION: This function is the message handler of this node.
 * 				This function does the following:
 * 				1) Pops messages from the queue
 * 				2) Handles the messages according to message types
 */
void MP2Node::checkMessages() {
	/*
	 * Implement this. Parts of it are already implemented
	 */
	char * data;
	int size;

	/*
	 * Declare your local variables here
	 */
       /* Remove timeout (stale) messages in the transaction log table */
        map<int, struct transaction>::iterator it;

        for ( it = transactionTable.begin(); it != transactionTable.end(); ++it )
        {
              if (par->getcurrtime() - it->second.timestamp > 6 )
              {
                  // timeout
                  MessageType type = it->second.transType;
                  switch (type)
                  {
                         case CREATE:
                              log->logCreateFail( &memberNode->addr, true, it->first, it->second.key, it->second.value );
                              break;
                         case READ:
                              log->logReadFail( &memberNode->addr, true, it->first, it->second.key );
                              break;
                         case UPDATE:
                              log->logUpdateFail( &memberNode->addr, true, it->first, it->second.key, it->second.value );
                              break;
                         case DELETE:
                              log->logDeleteFail( &memberNode->addr, true, it->first, it->second.key );
                              break;
                         default:
                              break;
                  }
#ifdef DEBUGLOG
                  log->LOG(&memberNode->addr, "removing transaction %d", it->first);
#endif
                  transactionTable.erase( it );
              }
        }

	// dequeue all messages and handle them
	while ( !memberNode->mp2q.empty() ) {
		/*
		 * Pop a message from the queue
		 */
		data = (char *)memberNode->mp2q.front().elt;
		size = memberNode->mp2q.front().size;
		memberNode->mp2q.pop();

		string message(data, data + size);
                Message transMessage(message);

		/*
		 * Handle the message types here
		 */
                 switch ( transMessage.type )
                 {
                         case CREATE:
                         {
                              bool success = createKeyValue(transMessage.key, transMessage.value, transMessage.replica); 
                              if (success)
                              {
                                   log->logCreateSuccess( &memberNode->addr, false, transMessage.transID, transMessage.key, transMessage.value );
                              }
                              else
                              {
                                   log->logCreateFail( &memberNode->addr, false, transMessage.transID, transMessage.key, transMessage.value );
                              }
                              Message MessageReply( transMessage.transID, memberNode->addr, REPLY, success );
                              message = MessageReply.toString();
                              emulNet->ENsend( &memberNode->addr, &transMessage.fromAddr, (char*) message.c_str(), message.size() );
                              break;
                         }
                         case READ:
                         {
                              string value = readKey(transMessage.key); 
                              if (!value.empty()) {
                                   log->logReadSuccess( &memberNode->addr, false, transMessage.transID, transMessage.key, value );
                              } 
                              else {
                                   log->logReadFail( &memberNode->addr, false, transMessage.transID, transMessage.key );
                              }
                              Message MessageReply( transMessage.transID, memberNode->addr, value );
                              message = MessageReply.toString();
                              emulNet->ENsend( &memberNode->addr, &transMessage.fromAddr, (char*) message.c_str(), message.size() );
                              break;
                         }
                         case UPDATE:
                         {
			      bool success = updateKeyValue(transMessage.key, transMessage.value, transMessage.replica); 
                              if (success)
                              {
                                   log->logUpdateSuccess( &memberNode->addr, false, transMessage.transID, transMessage.key, transMessage.value );
                              }
                              else
                              {
                                   log->logUpdateFail( &memberNode->addr, false, transMessage.transID, transMessage.key, transMessage.value );
                              }
                              Message MessageReply( transMessage.transID, memberNode->addr, REPLY, success );
                              message = MessageReply.toString();
                              emulNet->ENsend( &memberNode->addr, &transMessage.fromAddr, (char*) message.c_str(), message.size() );
                              break;
                         } 
                         case DELETE:
                         {
                              bool success = deleteKey( transMessage.key );
                              if (success)
                              {
                                   log->logDeleteSuccess( &memberNode->addr, false, transMessage.transID, transMessage.key );
                              }
                              else
                              {
                                   log->logDeleteFail( &memberNode->addr, false, transMessage.transID, transMessage.key);
                              }
                              Message MessageReply( transMessage.transID, transMessage.fromAddr, REPLY, success );
                              message = MessageReply.toString();
                              emulNet->ENsend( &memberNode->addr, &transMessage.fromAddr, (char*) message.c_str(), message.size() );
                              break;
                         }
                         case REPLY:
                         case READREPLY:
                         {
                              ReplyAndUpdateTable( transMessage );
                              break;
                         }
                         default:
                              break;
                 }
	}

	/*
	 * This function should also ensure all READ and UPDATE operation
	 * get QUORUM replies
	 */
}

/**
 * FUNCTION NAME: findNodes
 *
 * DESCRIPTION: Find the replicas of the given keyfunction
 * 				This function is responsible for finding the replicas of a key
 */
vector<Node> MP2Node::findNodes(string key) {
	size_t pos = hashFunction(key);
	vector<Node> addr_vec;
	if (ring.size() >= 3) {
		// if pos <= min || pos > max, the leader is the min
		if (pos <= ring.at(0).getHashCode() || pos > ring.at(ring.size()-1).getHashCode()) {
			addr_vec.emplace_back(ring.at(0));
			addr_vec.emplace_back(ring.at(1));
			addr_vec.emplace_back(ring.at(2));
		}
		else {
			// go through the ring until pos <= node
			for (unsigned int i=1; i<ring.size(); i++){
				Node addr = ring.at(i);
				if (pos <= addr.getHashCode()) {
					addr_vec.emplace_back(addr);
					addr_vec.emplace_back(ring.at((i+1)%ring.size()));
					addr_vec.emplace_back(ring.at((i+2)%ring.size()));
					break;
				}
			}
		}
	}
	return addr_vec;
}

/**
 * FUNCTION NAME: recvLoop
 *
 * DESCRIPTION: Receive messages from EmulNet and push into the queue (mp2q)
 */
bool MP2Node::recvLoop() {
    if ( memberNode->bFailed ) {
    	return false;
    }
    else {
    	return emulNet->ENrecv(&(memberNode->addr), this->enqueueWrapper, NULL, 1, &(memberNode->mp2q));
    }
}

/**
 * FUNCTION NAME: enqueueWrapper
 *
 * DESCRIPTION: Enqueue the message from Emulnet into the queue of MP2Node
 */
int MP2Node::enqueueWrapper(void *env, char *buff, int size) {
	Queue q;
	return q.enqueue((queue<q_elt> *)env, (void *)buff, size);
}

/**
 * FUNCTION NAME: findInFailedNodes
 *
 * DESCRIPTION: Is the node in the failed node list?
 */
bool MP2Node::findInFailedNodes( vector<Node>* failedNodes, size_t hashCode )
{
     for ( unsigned int i = 0; i < failedNodes->size(); i++ )
     {
           if ( (*failedNodes)[i].getHashCode() == hashCode )
                return true;
     }
     return false;
}
/**
 * FUNCTION NAME: findInMemberReplica
 *
 * DESCRIPTION: Is the node in the member node hasMyReplicas list?
 */
bool MP2Node::findInMemberReplica( Node node)
{
     Address *nodeAddr = node.getAddress();
     for ( unsigned int i = 0; i < hasMyReplicas.size(); i++ )
     {
           Address * addr = hasMyReplicas[i].getAddress();
           if ( (*(int *) &(nodeAddr->addr) == *(int *) &(addr->addr) ) &&
                (*(short *) &(nodeAddr->addr[4]) == *(short*) &(addr->addr[4])) )
                  return true;
     }
     return false;
}
/**
 * FUNCTION NAME: UpdateReplicaHt
 *
 * DESCRIPTION: Send message to the new replica to update its hashtable
 */
void MP2Node::UpdateReplicaHt( vector<Node> curMemList, int pos, int incr )
{

     ReplicaType type;
     if ( incr == 1 ) type = SECONDARY;
     if ( incr == 2 ) type = TERTIARY;

     Address* replicaAddress = curMemList[pos+ incr].getAddress();

     map<string, string>::iterator it;

     for (it = ht->hashTable.begin(); it != ht->hashTable.end(); ++it )
     {
          string key = it->first;
          string entryvalue = it->second;
          Entry entry ( it->second );

          Message copyMessage( ++g_transID, *replicaAddress, CREATE, key, entry.value, type); 
          string message = copyMessage.toString();
#ifdef DEBUGLOG
          log->LOG(&memberNode->addr, "UpdateReplicaHt: transID %d, key %s value %s ", g_transID, key.c_str(), entryvalue.c_str() );
#endif
          emulNet->ENsend( &memberNode->addr, replicaAddress, (char*) message.c_str(), message.size() );
          
      }
}
/**
 * FUNCTION NAME: stabilizationProtocol
 *
 * DESCRIPTION: This runs the stabilization protocol in case of Node joins and leaves
 * 				It ensures that there always 3 copies of all keys in the DHT at all times
 * 				The function does the following:
 *				1) Ensures that there are three "CORRECT" replicas of all the keys in spite of failures and joins
 *				Note:- "CORRECT" replicas implies that every key is replicated in its two neighboring nodes in the ring
 */
void MP2Node::stabilizationProtocol() {
	/*
	 * Implement this
	 */
        vector<Node> curMemList = getMembershipList();

	/* 1) Ensures that there are three "CORRECT" replicas of all the keys in spite of failures and joins
           Iterate the updated ring and update their neighbors
         */
         /* Where is member node in the ring */
         unsigned int indexInRing = 0;
         unsigned int indexInNewRing = 0;
         size_t memberHashCode = hashFunction(memberNode->addr.addr);
         for ( ; indexInRing < ring.size(); indexInRing++ )
         {
             if (memberHashCode == ring[indexInRing].getHashCode() )
                  break;
         }
         for ( ; indexInNewRing < curMemList.size(); indexInNewRing++ )
         {
             if (memberHashCode == curMemList[indexInNewRing].getHashCode() )
                  break;
         }

         vector<Node> failedNodes( sizeof(ring) ); 
         vector<Node>::iterator it = std::set_difference ( ring.begin(), ring.end(), curMemList.begin(), curMemList.end(), failedNodes.begin() );
         failedNodes.resize( it - failedNodes.begin() );

         bool nochange = true;
         if ( indexInRing < ring.size()  && indexInNewRing < curMemList.size() )
         {
              // run thru the replica to find fail node
              for (int i = 1; i < 3; i++ )
              {
                   if (findInFailedNodes( &failedNodes, ring[indexInRing + i].getHashCode()))
                   {
                        nochange = false;
                   } 
              }
         }

         if ( nochange ) return;
         // ring has changed, update Replica
         for ( int i = 1; i < 3; i++ )
         {
               if ( !findInMemberReplica( curMemList[indexInNewRing + i ] ))
               {
                    // only one failed replica needs updated since 2 is enough to make a quorum
                    UpdateReplicaHt( curMemList, indexInNewRing, i );
               }
         }
}

void MP2Node::UpdateReplicaList()
{
         unsigned int index = 0;
         size_t memberHashCode = hashFunction(memberNode->addr.addr);
         for ( ; index < ring.size(); index++ )
         {
             if (memberHashCode == ring[index].getHashCode() )
                  break;
         }
         /* Update the content of the hashtable to the new pred and successor */
         /* Lastly update the hasMyReplicas field */
         hasMyReplicas.clear();
         int repindex = ( index + 1 ) < ring.size() ? index + 1 : index + 1 - ring.size();
         hasMyReplicas.emplace_back( ring[ repindex ]);
         repindex = ( index + 2 ) < ring.size() ? index + 2 : index + 2 -ring.size();
         hasMyReplicas.emplace_back( ring[repindex ]);    

         /* Lastly update the haveReplicasOf field */
         haveReplicasOf.clear();
         repindex = ( index - 1 ) < 0 ? ring.size() + index -1 : index -1 ;
         haveReplicasOf.emplace_back( ring[repindex] );
         repindex = ( index - 2 ) < 0 ? ring.size() + index -2 : index -1 ;
         haveReplicasOf.emplace_back( ring[repindex] );
}

char* MP2Node::getIP( Address * addr )
{
         static char stdstring[40];

         sprintf(stdstring, "%d.%d.%d.%d:%d ", addr->addr[0], addr->addr[1], addr->addr[2], addr->addr[3], *(short *)&addr->addr[4]); 

         return stdstring;
}
