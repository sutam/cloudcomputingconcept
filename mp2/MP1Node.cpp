/**********************************
 * FILE NAME: MP1Node.cpp
 *
 * DESCRIPTION: Membership protocol run by this Node.
 * 				Definition of MP1Node class functions.
 **********************************/

#include "MP1Node.h"
/*
 * Note: You can change/add any functions in MP1Node.{h,cpp}
 */

/**
 * Overloaded Constructor of the MP1Node class
 * You can add new members to the class if you think it
 * is necessary for your logic to work
 */
MP1Node::MP1Node(Member *member, Params *params, EmulNet *emul, Log *log, Address *address) {
	for( int i = 0; i < 6; i++ ) {
		NULLADDR[i] = 0;
	}
	this->memberNode = member;
	this->emulNet = emul;
	this->log = log;
	this->par = params;
	this->memberNode->addr = *address;
}

/**
 * Destructor of the MP1Node class
 */
MP1Node::~MP1Node() {}

/**
 * FUNCTION NAME: recvLoop
 *
 * DESCRIPTION: This function receives message from the network and pushes into the queue
 * 				This function is called by a node to receive messages currently waiting for it
 */
int MP1Node::recvLoop() {
    if ( memberNode->bFailed ) {
    	return false;
    }
    else {
    	return emulNet->ENrecv(&(memberNode->addr), enqueueWrapper, NULL, 1, &(memberNode->mp1q));
    }
}

/**
 * FUNCTION NAME: enqueueWrapper
 *
 * DESCRIPTION: Enqueue the message from Emulnet into the queue
 */
int MP1Node::enqueueWrapper(void *env, char *buff, int size) {
	Queue q;
	return q.enqueue((queue<q_elt> *)env, (void *)buff, size);
}

/**
 * FUNCTION NAME: nodeStart
 *
 * DESCRIPTION: This function bootstraps the node
 * 				All initializations routines for a member.
 * 				Called by the application layer.
 */
void MP1Node::nodeStart(char *servaddrstr, short servport) {
    Address joinaddr;
    joinaddr = getJoinAddress();

    // Self booting routines
    if( initThisNode(&joinaddr) == -1 ) {
#ifdef DEBUGLOG
        log->LOG(&memberNode->addr, "init_thisnode failed. Exit.");
#endif
        exit(1);
    }

    if( !introduceSelfToGroup(&joinaddr) ) {
        finishUpThisNode();
#ifdef DEBUGLOG
        log->LOG(&memberNode->addr, "Unable to join self to group. Exiting.");
#endif
        exit(1);
    }

    return;
}

/**
 * FUNCTION NAME: initThisNode
 *
 * DESCRIPTION: Find out who I am and start up
 */
int MP1Node::initThisNode(Address *joinaddr) {
	/*
	 * This function is partially implemented and may require changes
	 */
	int id = *(int*)(&memberNode->addr.addr);
	int port = *(short*)(&memberNode->addr.addr[4]);

	memberNode->bFailed = false;
	memberNode->inited = true;
	memberNode->inGroup = false;
    // node is up!
	memberNode->nnb = 0;
	memberNode->heartbeat = 0;
	memberNode->pingCounter = TFAIL;
	memberNode->timeOutCounter = -1;
    initMemberListTable(memberNode);

    return 0;
}

/**
 * FUNCTION NAME: introduceSelfToGroup
 *
 * DESCRIPTION: Join the distributed system
 */
int MP1Node::introduceSelfToGroup(Address *joinaddr) {
	MessageHdr *msg;
#ifdef DEBUGLOG
    static char s[1024];
#endif

    if ( 0 == memcmp((char *)&(memberNode->addr.addr), (char *)&(joinaddr->addr), sizeof(memberNode->addr.addr))) {
        // I am the group booter (first process to join the group). Boot up the group
#ifdef DEBUGLOG
        log->LOG(&memberNode->addr, "Starting up group...");
#endif
        memberNode->inGroup = true;
    }
    else {
        size_t msgsize = sizeof(MessageHdr) + sizeof(joinaddr->addr) + sizeof(long) + 1;
        msg = (MessageHdr *) malloc(msgsize * sizeof(char));

        // create JOINREQ message: format of data is {struct Address myaddr}
        msg->msgType = JOINREQ;
        memcpy((char *)(msg+1), &memberNode->addr.addr, sizeof(memberNode->addr.addr));
        memcpy((char *)(msg+1) + sizeof(memberNode->addr.addr), &memberNode->heartbeat, sizeof(long));

#ifdef DEBUGLOG
        sprintf(s, "Trying to join...");
        log->LOG(&memberNode->addr, s);
#endif

        // send JOINREQ message to introducer member
        emulNet->ENsend(&memberNode->addr, joinaddr, (char *)msg, msgsize);

        free(msg);
    }

    return 1;

}

/**
 * FUNCTION NAME: finishUpThisNode
 *
 * DESCRIPTION: Wind up this node and clean up state
 */
int MP1Node::finishUpThisNode(){
   /*
    * Your code goes here
    */
   return 0;
}

/**
 * FUNCTION NAME: nodeLoop
 *
 * DESCRIPTION: Executed periodically at each member
 * 				Check your messages in queue and perform membership protocol duties
 */
void MP1Node::nodeLoop() {
    if (memberNode->bFailed) {
    	return;
    }

    // Check my messages
    checkMessages();

    // Wait until you're in the group...
    if( !memberNode->inGroup ) {
    	return;
    }

    // ...then jump in and share your responsibilites!
    nodeLoopOps();

    return;
}

/**
 * FUNCTION NAME: checkMessages
 *
 * DESCRIPTION: Check messages in the queue and call the respective message handler
 */
void MP1Node::checkMessages() {
    void *ptr;
    int size;

    // Pop waiting messages from memberNode's mp1q
    while ( !memberNode->mp1q.empty() ) {
    	ptr = memberNode->mp1q.front().elt;
    	size = memberNode->mp1q.front().size;
    	memberNode->mp1q.pop();
    	recvCallBack((void *)memberNode, (char *)ptr, size);
    }
    return;
}

const char *msgTypeToString( MsgTypes type )
{
      switch (type )
      {
	case JOINREQ:
             return "JOINREQ";
        case JOINREP:
             return "JOINREP";
        case HBREQ:
             return "HBREQ";
        case HBREP:
             return "HBREP";
        default:
             return "Null Message Type";
      }
}

char* MP1Node::makerow( 	char* table,  MemberListEntry& entry )
{
#ifdef DEBUGLOG
               //log->LOG( &memberNode->addr, "makerow: node id %d port %d heartbeat %d",
                //                     entry.id, entry.port, entry.heartbeat );
#endif
      memcpy( table , &entry.id, sizeof( int ) );
      memcpy( table += sizeof( int), &entry.port, sizeof(short));
      memcpy( table += sizeof( short), &entry.heartbeat, sizeof(long) );
      return table + sizeof(long);
}

bool MP1Node::sendMsgToDstAddr( Member* memberNode, Address *repaddr, MsgTypes msgType ) {

#ifdef DEBUGLOG
     char s[1024];
     int id     = *(int *) ( &repaddr->addr );
     short port = *(short *) ( &repaddr->addr[4]);
     //sprintf(s, "sendMsgToDstAddr: Sending %s messages to address id %d port %d...", msgTypeToString(msgType), id, port );
     //log->LOG(&memberNode->addr, s);
#endif
     // create message: format of data is {Address senderaddr
     //                                    NumberMembers
     //                                    memberlist info}

     //  number of members in MemberList
     int numMembers = memberNode->memberList.size();
     size_t sendersize = sizeof(memberNode->addr.addr ) + sizeof(long);
     //  int id;
     //  short port;
     //  long heartbeat;
     size_t msgbodysize = sizeof(int) + sizeof(short) + sizeof(long);
     size_t msgsize = sizeof(MessageHdr) + sendersize + numMembers * msgbodysize;
     MessageHdr *msg = (MessageHdr *) malloc(msgsize * sizeof(char));
     msg->msgType = msgType;
     char* senderTableBegin = (char*) (msg + 1) + sendersize;
     char* currow = senderTableBegin;

     // Starting from the position of member list
     vector<MemberListEntry>::iterator it ;
     for ( it = memberNode->memberList.begin() ; it != memberNode->memberList.end(); ) {
           currow = makerow( currow, *it );
           ++it;
     }
     memcpy((char *)(msg+1), &memberNode->addr.addr, sizeof(memberNode->addr.addr));
     memcpy((char *)(msg+1) + sizeof(memberNode->addr.addr), &numMembers, sizeof(long));
#ifdef DEBUGLOG
     //log->LOG(&memberNode->addr, "sendMsgToDstAddr: Message sent %s", msg );
#endif
     emulNet->ENsend(&memberNode->addr, repaddr, (char *)msg, msgsize);
     free (msg);
     return true;
}

bool MP1Node::recvMsgFromSrcAddr ( MsgTypes msgType, char* msg  )
{
     long numMembers;
     memcpy( &numMembers, msg , sizeof( long ));
#ifdef DEBUGLOG
     char s[1024];
     //sprintf(s, "recvMsgFromSrcAddr: Receiving %s messages...", msgTypeToString(msgType) );
     //log->LOG(&memberNode->addr, s);
#endif
#if 0
     if ((sizeof(msg) - sizeof(long)) < 
              numMembers * (sizeof(int) + sizeof(short) + sizeof(long)))
     {
           log->LOG(&memberNode->addr, "Message %s received truncated",
                                msgTypeToString( msgType ));
           return false;
     }
#endif
     for ( int i = 0; i < numMembers; i++ )
     {
           int id;
           short port;
           long heartbeat, timestamp;
           memcpy( &id, msg += sizeof(long), sizeof(int) );
           memcpy( &port, msg += sizeof(int), sizeof(short) );
           memcpy( &heartbeat, msg += sizeof(short), sizeof(long) );
           MemberUpdate(id, port, heartbeat);
    }
    return true;
}

bool MP1Node::recvJOINREQ(void *env, char *data, int size ) {

     long heartbeat;
     Address fromaddr;
     memcpy( fromaddr.addr, data , sizeof( fromaddr.addr ));
     memcpy( &heartbeat, data + sizeof(fromaddr.addr), sizeof ( long ));
     int id     = *(int *) ( &fromaddr.addr );
     short port = *(short *) ( &fromaddr.addr[4]);

#ifdef DEBUGLOG
     //log->LOG(&memberNode->addr, "Receive JOINREQ request from id %d port %d...", id, port);
#endif
     // Replying to the message sender by sending message back to sender
     bool retcode = sendMsgToDstAddr( memberNode, &fromaddr, JOINREP );

     MemberUpdate(id, port, heartbeat);

     return retcode;
}

bool MP1Node::recvJOINREP(void *env, char *data, int size ) {

     Address fromaddr;
     memcpy( fromaddr.addr, data , sizeof( fromaddr.addr ));
#ifdef DEBUGLOG
     int id = *(int*)(&fromaddr.addr);
     int port = *(short*)(&fromaddr.addr[4]);
     //log->LOG(&memberNode->addr, "Receive JOINREP from address id %d port %d ...", id, port);
#endif
     bool retcode = recvMsgFromSrcAddr ( JOINREP,  data + sizeof(fromaddr.addr));
     if (retcode)
         memberNode->inGroup = true;
     return retcode;
}

bool MP1Node::recvHBREP(void *env, char *data, int size ) {

     Address fromaddr;
     long heartbeat;
     memcpy( fromaddr.addr, data , sizeof( fromaddr.addr ));
     memcpy( &heartbeat, data+sizeof( fromaddr.addr), sizeof(long) ); 
     int id     = *(int *) ( &fromaddr.addr );
     short port = *(short *) ( &fromaddr.addr[4]);
     MemberUpdate(id, port, heartbeat);
#ifdef DEBUGLOG
     //log->LOG(&memberNode->addr, "Receive heartbeat request from address id %d port %d...", id, port);
#endif
     recvMsgFromSrcAddr ( HBREP, data + sizeof(memberNode->addr.addr  ));
}

bool MP1Node::recvHBREQ(void *env, char *data, int size ) {
      return true;
}
/**
 * FUNCTION NAME: recvCallBack
 *
 * DESCRIPTION: Message handler for different message types
 */
bool MP1Node::recvCallBack(void *env, char *data, int size ) {
	/*
	 * Your code goes here
	 */
	bool status;
        if ( size < sizeof( MessageHdr ) + sizeof(memberNode->addr) + sizeof(long)  )
        {
#ifdef DEBUGLOG
            log->LOG( &memberNode->addr, "Received message is defective or truncated");
#endif
            return false;
        }
	// Check the message type
        MessageHdr * msg = (MessageHdr * ) data;
        MsgTypes msgtype = msg->msgType;
	switch (msgtype)
	{
		// implement JOINREQ
                case JOINREQ:
			status = recvJOINREQ( env,(char*) (msg + 1) , size - sizeof( MessageHdr) );
			break;
		case JOINREP:
			status = recvJOINREP( env, (char*) (msg + 1) , size - sizeof( MessageHdr) );
			break;
		case HBREQ:
			status = recvHBREQ ( env, (char*) (msg + 1) , size );
			break;
		case HBREP:
                        status = recvHBREP ( env, (char*) (msg + 1), size );
			break;
		default:
#ifdef DEBUGLOG
			log->LOG( &memberNode->addr, "Received message does not have any legal message type.\n");
#endif
			status = false;
	}
	return status;
}

/**
 * FUNCTION NAME: nodeLoopOps
 *
 * DESCRIPTION: Check if any node hasn't responded within a timeout period and then delete
 * 				the nodes
 * 				Propagate your membership list
 */
void MP1Node::nodeLoopOps() {

	/*
	 * Your code goes here
	 */

         // Check if any node hasn't responded within a timeout period and then delete
         for (vector<MemberListEntry>::iterator it = memberNode->memberList.begin(); it != memberNode->memberList.end();  ) {
              // Do not deal with memberNode itself
           if ( it != memberNode->memberList.begin()) {
                if (par->getcurrtime() - it->timestamp > TREMOVE )
                {
                 // Remove the node
#ifdef DEBUGLOG
                    Address removeNode;
                    memcpy(& removeNode.addr[0], &it->id, sizeof(int));
                    memcpy(& removeNode.addr[4], &it->port, sizeof(short));
 		    log->logNodeRemove(&memberNode->addr, &removeNode);
#endif
                    it = memberNode->memberList.erase(it);
                    continue;
                }
            }
            ++it;
         }
         memberNode->heartbeat++;
         // update heartbeat for current node in member list
         vector<MemberListEntry>::iterator currentnode;
         currentnode = memberNode->memberList.begin();
         currentnode->heartbeat++;
         currentnode->timestamp = par->getcurrtime();
         // Count down on the ping from 5
         // If count down to 0, we spread the gossip to all members
         memberNode->pingCounter--;
         if ( memberNode->pingCounter == 0 )
         {
              for (vector<MemberListEntry>::iterator it = memberNode->memberList.begin(); it != memberNode->memberList.end(); ++it ) {
                   Address addr = getAddress ( it->id, it->port );
                   sendMsgToDstAddr( memberNode, &addr, HBREP );
              }
              memberNode->pingCounter = TFAIL;
         }
         

         return;
}

/**
 * FUNCTION NAME: isNullAddress
 *
 * DESCRIPTION: Function checks if the address is NULL
 */
int MP1Node::isNullAddress(Address *addr) {
	return (memcmp(addr->addr, NULLADDR, 6) == 0 ? 1 : 0);
}
/**
 * FUNCTION NAME: getAddress
 *
 * DESCRIPTION: Returns the Address given id and port
 */
Address MP1Node::getAddress( int id, short port ) {
    Address addr;

    memset(&addr, 0, sizeof(Address));
    *(int *)(&addr.addr) = id;
    *(short *)(&addr.addr[4]) = port;

    return addr;
}

/**
 * FUNCTION NAME: getJoinAddress
 *
 * DESCRIPTION: Returns the Address of the coordinator
 */
Address MP1Node::getJoinAddress() {
    Address joinaddr;

    memset(&joinaddr, 0, sizeof(Address));
    *(int *)(&joinaddr.addr) = 1;
    *(short *)(&joinaddr.addr[4]) = 0;

    return joinaddr;
}

/**
 * FUNCTION NAME: initMemberListTable
 *
 * DESCRIPTION: Initialize the membership list
 */
void MP1Node::initMemberListTable(Member *memberNode) {

	memberNode->memberList.clear();
	// We need to update the memberList to include the 
        // current node
	int id     = *(int *) (&memberNode->addr);
        short port = *(short *) (&memberNode->addr.addr[4]);
        long heartbeat = *( long *) (&memberNode->heartbeat);
        MemberListEntry entry( id, port, heartbeat, par->getcurrtime());
        memberNode->memberList.push_back(entry);

#ifdef DEBUGLOG
        //log->LOG(&memberNode->addr, "first member in member list");
#endif
        log->logNodeAdd(&memberNode->addr, &memberNode->addr);
        memberNode->myPos = memberNode->memberList.begin();
}

/**
 * FUNCTION NAME: MemberListTableUpdate
 *
 * DESCRIPTION: Update the membership list
 */
void MP1Node::MemberListTableUpdate(int id,  short port, long heartbeat ) {

        MemberListEntry entry( id, port, heartbeat, par->getcurrtime() );
        memberNode->memberList.push_back(entry);
#ifdef DEBUGLOG
        //log->LOG(&memberNode->addr, "fellow member in member list");
        Address newMember;
        memcpy( &newMember.addr[0], &id, sizeof(int));
        memcpy( &newMember.addr[4], &port, sizeof(short));
	log->logNodeAdd(&memberNode->addr, &newMember);
#endif
}

void MP1Node::MemberUpdate( int id, short port, long heartbeat ) {

        vector<MemberListEntry>::iterator entry = memberNode->memberList.begin();
	for ( ; entry != memberNode->memberList.end(); ++ entry ) {
                if ( entry->id == id && entry->port == port ) {
                     if ( entry->heartbeat < heartbeat )
                     {
#ifdef DEBUGLOG
			  //log->LOG( &memberNode->addr, "MemberUpdate with id %d port %d heartbeat ( %d -> %d ) and timestamp( %d -> %d ).\n", id, port, entry->heartbeat, heartbeat, entry->timestamp, par->getcurrtime());
#endif
                          entry->setheartbeat( heartbeat);
                          entry->settimestamp( par->getcurrtime());
                     }
                     return;
                }
        }
        MemberListTableUpdate( id, port, heartbeat );
}

/**
 * FUNCTION NAME: printAddress
 *
 * DESCRIPTION: Print the Address
 */
void MP1Node::printAddress(Address *addr)
{
    printf("%d.%d.%d.%d:%d \n",  addr->addr[0],addr->addr[1],addr->addr[2],
                                                       addr->addr[3], *(short*)&addr->addr[4]) ;    
}
